package com.comulynx.wallet.rest.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.comulynx.wallet.rest.api.exception.ResourceNotFoundException;
import com.comulynx.wallet.rest.api.model.Webuser;
import com.comulynx.wallet.rest.api.repository.WebuserRepository;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1/webusers")
public class WebuserController {
	private Gson gson = new Gson();

	@Autowired
	private WebuserRepository webuserRepository;

	/**
	 * TODO : Fix Webuser Login functionality
	 * 
	 * Login
	 * 
	 * @param request
	 * @return
	 */
	

	
	
	
	
	
	@PostMapping("/login")
	public ResponseEntity<?> webuserLogin(@RequestBody String request,HashMap<String, String> webUser) {
		try {


			// TODO : Add Webuser login logic here. Login using employeeId and
			// password
			//NB: We are using plain text password for testing Webuser login
			//If username doesn't exists throw an error "User does not exist"
			//If password do not match throw an error "Invalid credentials"
			
			final JsonObject userRequest = gson.fromJson(request, JsonObject.class);
			String employeeId = userRequest.get("employeeId").getAsString();
			String password = userRequest.get("password").getAsString();
			
			
			 
			Webuser webusers = webuserRepository.findByEmployeeId(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Webuser not found for this Employee Id :: " + employeeId));
			
			
			
			String pass = webusers.getPassword();
			 
			 if(password.equals(pass) ) {
				 
				 return ResponseEntity.status(200).body(HttpStatus.OK);	
				
			 }else {
				 throw new ValidationException("Invalid Credentials"); 
			 }

//			 return ResponseEntity.status(200).body(HttpStatus.OK);			 
//			 return ResponseEntity.status(200).body(password);

		} catch (Exception ex) {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

		}
	

  }

	@GetMapping("/")
	public List<Webuser> getAllWebusers() {
		return webuserRepository.findAll();
	}

	@GetMapping("/{employeeId}")
	public ResponseEntity<Webuser> getWebuserByEmployeeId(@PathVariable(value = "employeeId") String employeeId)
			throws ResourceNotFoundException {
		Webuser webuser = webuserRepository.findByEmployeeId(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Webuser not found for this id :: " + employeeId));
		return ResponseEntity.ok().body(webuser);
	}

	@PostMapping("/")
	public ResponseEntity<?> createWebuser(@Valid @RequestBody Webuser webuser) {
		try {
			// TODO : Add logic to check if Webuser with provided username, or
			// email, or employeeId, or customerId exists.
			// If exists, throw a Webuser with [?] exists Exception.
			String username = webuser.getUsername();
			String email = webuser.getEmail();
			String employeeId =webuser.getEmployeeId();
			String customerId = webuser.getCustomerId();
			
			  if (webuserRepository.existsByUsername(username)){

		            throw new ValidationException("User with the username " + username +"   Exist");

		        }
			  else if (webuserRepository.existsByEmail(email)){

		            throw new ValidationException("User with the email " + email + " Exist");

		        }
			  else if (webuserRepository.existsByEmployeeId(employeeId) ){

		            throw new ValidationException("User with the employeeId " + employeeId + "  Exist");

		        }
			  else if (webuserRepository.existsByCustomerId(customerId)){

		            throw new ValidationException("User with the customerId "+ customerId + " Exist");

		        }
			
				 
				 webuser.setCustomerId(webuser.getCustomerId()); 
				 webuser.setEmployeeId(webuser.getEmployeeId());
				 webuser.setUsername(webuser.getUsername());
				 webuser.setPassword(webuser.getPassword());
				 webuser.setEmail(webuser.getEmail());
				 webuser.setLastName(webuser.getLastName());
				webuser.setFirstName(webuser.getFirstName());
					
					return ResponseEntity.ok().body(webuserRepository.save(webuser));
				 
			 
			
	
			
		} catch (Exception ex) {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	@PutMapping("/{employeeId}")
	public ResponseEntity<Webuser> updateWebuser(@PathVariable(value = "employeeId") String employeeId,
			@Valid @RequestBody Webuser webuserDetails) throws ResourceNotFoundException {
		Webuser webuser = webuserRepository.findByEmployeeId(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Webuser not found for this id :: " + employeeId));

		webuser.setEmail(webuserDetails.getEmail());
		webuser.setLastName(webuserDetails.getLastName());
		webuser.setFirstName(webuserDetails.getFirstName());
		final Webuser updatedWebuser = webuserRepository.save(webuser);
		return ResponseEntity.ok(updatedWebuser);
	}

	@DeleteMapping("/{employeeId}")
	public Map<String, Boolean> deleteWebuser(@PathVariable(value = "employeeId") String employeeId)
			throws ResourceNotFoundException {
		Webuser webuser = webuserRepository.findByEmployeeId(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Webuser not found for this id :: " + employeeId));

		webuserRepository.delete(webuser);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
