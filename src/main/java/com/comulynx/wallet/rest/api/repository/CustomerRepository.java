package com.comulynx.wallet.rest.api.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.comulynx.wallet.rest.api.model.Customer;


@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	

	 @Query(value = "SELECT c.CUSTOMER_ID ,c.EMAIL,c.FIRST_NAME,c.LAST_NAME, a.ACCOUNT_NO, a.BALANCE   FROM CUSTOMERS c INNER JOIN ACCOUNTS a ON c.CUSTOMER_ID = a.CUSTOMER_ID", nativeQuery = true)
	 	List<Map<String, String>> getFromCustomerAndAccountDetails();

	@Query(value = "SELECT c.CUSTOMER_ID ,c.EMAIL,c.FIRST_NAME,c.LAST_NAME, a.ACCOUNT_NO, a.BALANCE   FROM CUSTOMERS c INNER JOIN ACCOUNTS a ON c.CUSTOMER_ID = a.CUSTOMER_ID WHERE c.CUSTOMER_ID = :customer_id", nativeQuery = true)
	List<Map<String, String>> getFromCustomerAndAccountDetailsPerCustomer(@Param("customer_id") String customer_id);

	Optional<Customer> findByCustomerId(String customerId);
	
	boolean existsByUsername(String username);
	boolean existsByCustomerId(String customerId);

	// TODO : Implement the query and function below to delete a customer using Customer Id
	@Modifying(clearAutomatically = true) 
	@Transactional
	
	 @Query("DELETE  from  Customer c where c.customerId =:customerId")
	 int  deleteCustomerByCustomerId( @Param("customerId") String customer_id);
	

	// TODO : Implement the query and function below to update customer firstName using Customer Id
	// @Query("?")
	// int updateCustomerByCustomerId(String firstName, String customer_id);
	  
	@Modifying(clearAutomatically = true) 
	@Transactional
	 @Query("update Customer u set u.firstName = :firstName where u.customerId = :customerId")
	 int updateUserSetStatusForName(@Param("firstName") String firstName, 
	   @Param("customerId") String customer_id);
	
	
	
	// TODO : Implement the query and function below and to return all customers whose Email contains  'gmail'
	// @Query("?")
	// List<Customer> findAllCustomersWhoseEmailContainsGmail();
	
	@Query("SELECT c FROM Customer c WHERE c.email LIKE '%gmail%'")
	 List<Customer> findAllCustomersWhoseEmailContainsGmail();
     
	
}
